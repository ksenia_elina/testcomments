<?php
class Model_CoursesAdd extends Model
{
    public $dbh;
    //получаем из базы
    public function get_data()
    {
        $sth = $this->dbh->prepare("SELECT * FROM `Messages`.`Messages` order by date DESC;");
        $sth->execute();
        $res = $sth->get_result();
        $result = $res->fetch_all();
        //var_dump($result);
        return $result;
    }
    //добавляем в базу
     public function add_data(array $mas, string $Date)
    {
        $sth = $this->dbh->prepare("INSERT INTO `Messages`.`Messages` (id, name, date, text, email) VALUES (NULL, ?, ?, ?,?);");
        mysqli_stmt_bind_param($sth, 'ssss', htmlspecialchars($mas['name']),$Date, htmlspecialchars($mas['comment']), htmlspecialchars($mas['Email'] ));
        $sth->execute();
        $res = $sth->get_result();
        //$result = $res->fetch_all();
        //var_dump($result);
        return $result;
    }

}
?>
