-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 28 2020 г., 11:06
-- Версия сервера: 5.6.41
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `messages`
--

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date` timestamp(6) NOT NULL,
  `text` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `name`, `date`, `text`, `email`) VALUES
(1, 'Ksu', '2020-09-28 10:57:47.781999', 'Все работает', ''),
(2, 'Александра', '0000-00-00 00:00:00.000000', '123', ''),
(3, 'Александра', '0000-00-00 00:00:00.000000', '123', ''),
(4, 'Александра', '0000-00-00 00:00:00.000000', '123', ''),
(5, 'Slimpay', '0000-00-00 00:00:00.000000', '123444', ''),
(6, 'phpJsonSlim', '0000-00-00 00:00:00.000000', '1', ''),
(7, 'phpJsonSlim', '0000-00-00 00:00:00.000000', '1', ''),
(8, 'phpJsonSlim', '2020-09-28 12:45:40.000000', '1', ''),
(9, 'phpJsonSlim', '2020-09-28 12:45:49.000000', '1', ''),
(11, 'Slimpay', '2020-09-29 13:16:35.000000', '&lt;script&gt;&lt;/script&gt;', ''),
(12, 'Александра', '2020-09-29 13:17:02.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(13, 'Александра', '2020-09-29 13:19:57.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(14, 'Александра', '2020-09-29 13:20:09.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(15, 'Александра', '2020-09-29 13:20:42.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(16, 'Александра', '2020-09-29 13:21:44.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(17, 'Александра', '2020-09-29 13:22:22.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(18, 'Александра', '2020-09-29 13:22:57.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(19, 'Александра', '2020-09-29 13:27:21.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(20, 'Александра', '2020-09-29 13:27:43.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(21, 'Александра', '2020-09-29 13:32:19.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(22, 'Александра', '2020-09-29 13:32:33.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(23, 'Александра', '2020-09-29 13:32:51.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(24, 'Александра', '2020-09-29 13:33:13.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(25, 'Александра', '2020-09-29 13:33:30.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(26, 'Александра', '2020-09-29 13:33:45.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(27, 'Александра', '2020-09-29 13:34:14.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(28, 'Александра', '2020-09-29 13:37:29.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(29, 'Александра', '2020-09-29 13:38:40.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(30, 'Александра', '2020-09-29 13:49:05.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(31, 'Александра', '2020-09-29 13:50:18.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(32, 'Александра', '2020-09-29 13:51:00.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(33, 'Александра', '2020-09-29 13:51:33.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(34, 'Александра', '2020-09-29 13:52:43.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(35, 'Александра', '2020-09-29 14:07:14.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(36, 'Александра', '2020-09-29 14:15:32.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(37, 'Александра', '2020-09-29 14:15:52.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;', ''),
(38, '123', '2020-10-26 17:19:07.000000', '1', ''),
(39, 'Александра', '2020-10-26 17:30:03.000000', '22', ''),
(40, 'Александра', '2020-10-26 17:31:16.000000', '22', ''),
(41, 'Slimpay', '2020-10-26 17:31:24.000000', '111', ''),
(42, 'Slimpay', '2020-10-26 17:32:06.000000', '111', ''),
(43, 'Slimpay', '2020-10-26 17:33:35.000000', '111', ''),
(44, 'Slimpay', '2020-10-26 17:34:04.000000', '111', ''),
(45, 'Slimpay', '2020-10-26 17:36:58.000000', '111', ''),
(46, 'Slimpay', '2020-10-26 17:37:39.000000', '111', ''),
(47, 'Slimpay', '2020-10-26 17:40:51.000000', '111', ''),
(48, 'Slimpay', '2020-10-26 17:43:12.000000', '111', ''),
(49, 'Slimpay', '2020-10-26 17:43:33.000000', '111', ''),
(50, 'Александра', '2020-10-26 17:43:39.000000', '11', ''),
(51, 'Александра', '2020-10-26 17:43:48.000000', '11444', ''),
(52, 'Slimpay', '2020-10-26 17:43:54.000000', '111', ''),
(53, 'Slimpay', '2020-10-26 17:45:03.000000', '111', ''),
(54, 'Slimpay', '2020-10-26 17:45:31.000000', '111', ''),
(55, 'Slimpay', '2020-10-26 17:46:01.000000', '111', ''),
(56, 'Slimpay', '2020-10-26 17:46:09.000000', '11', ''),
(57, 'Slimpay', '2020-10-26 17:46:19.000000', '113453', ''),
(58, 'Slimpay', '2020-10-26 17:46:30.000000', '113453', ''),
(59, 'Slimpay', '2020-10-27 16:20:07.000000', '113453', ''),
(60, 'Slimpay', '2020-10-27 16:20:09.000000', '113453', ''),
(61, 'Slimpay', '2020-10-27 16:20:09.000000', '111', ''),
(62, 'Александра', '2020-10-27 16:20:35.000000', '12121', ''),
(63, 'Slimpay', '2020-10-27 16:40:13.000000', '111', ''),
(64, 'Александра', '2020-10-27 16:40:24.000000', '11', ''),
(65, 'Александра', '2020-10-27 16:40:54.000000', '11', ''),
(66, 'Slimpay', '2020-10-27 16:41:30.000000', '111', ''),
(67, 'Александра', '2020-10-27 16:41:37.000000', '1212', ''),
(68, 'Slimpay', '2020-10-27 16:43:09.000000', '111', ''),
(69, 'Александра', '2020-10-27 16:43:15.000000', '11', ''),
(70, 'Александра', '2020-10-27 16:43:21.000000', '11', ''),
(71, 'Александра', '2020-10-27 16:44:03.000000', '11', ''),
(72, 'Александра', '2020-10-27 16:44:06.000000', '11', ''),
(73, 'Slimpay', '2020-10-27 16:45:28.000000', '111', ''),
(74, 'Александра', '2020-10-27 16:46:20.000000', '1212', ''),
(75, 'Александра', '2020-10-27 16:48:17.000000', '1212', ''),
(76, 'Александра', '2020-10-27 16:48:20.000000', '1212', ''),
(77, 'Slimpay', '2020-10-27 16:54:52.000000', '111', ''),
(78, 'Slimpay', '2020-10-27 16:55:00.000000', 'q1', ''),
(79, 'Slimpay', '2020-10-27 16:56:14.000000', 'q1', ''),
(80, 'Slimpay', '2020-10-27 16:56:21.000000', 'q1', ''),
(81, 'Slimpay', '2020-10-27 16:59:10.000000', 'q1', ''),
(82, 'Slimpay', '2020-10-27 17:03:23.000000', '111', ''),
(83, 'Александра', '2020-10-27 17:03:29.000000', '11', ''),
(84, 'Александра', '2020-10-27 17:04:16.000000', '11', ''),
(85, 'Slimpay', '2020-10-27 17:04:36.000000', '111', ''),
(86, 'Александра', '2020-10-27 17:04:42.000000', '11', ''),
(87, 'Slimpay', '2020-10-27 17:05:48.000000', '111', ''),
(88, 'Александра', '2020-10-27 17:05:54.000000', '12', ''),
(89, 'Александра', '2020-10-27 17:06:03.000000', '12', ''),
(90, 'Slimpay', '2020-10-27 17:09:12.000000', '111', ''),
(91, 'Александра', '2020-10-27 17:09:21.000000', '121', ''),
(92, 'Slimpay', '2020-10-27 17:10:20.000000', '11', ''),
(93, '1', '2020-10-27 17:11:08.000000', '111', ''),
(94, 'Александра', '2020-10-27 17:39:14.000000', '1212', ''),
(95, '1', '2020-10-28 07:37:47.000000', '11()', ''),
(96, '', '2020-10-28 07:39:17.000000', '', ''),
(97, '', '2020-10-28 07:51:37.000000', '123', ''),
(98, '', '2020-10-28 07:51:58.000000', '123', ''),
(99, '', '2020-10-28 07:52:17.000000', 'im!', ''),
(100, '', '2020-10-28 07:52:25.000000', '!/&lt;&gt;', ''),
(101, '12,', '2020-10-28 07:55:56.000000', '12w', ''),
(102, '12,', '2020-10-28 07:56:15.000000', '12w', ''),
(103, 'Александра', '2020-10-28 08:02:43.000000', '121', ''),
(104, 'Александра', '2020-10-28 08:05:02.000000', '12132', '121@ya.ru');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
