<?php
class Controller_Messages extends Controller
{

	function __construct()
	{
		$this->model = new Model_CoursesAdd();
		$this->view = new View();
	}
	//загрузка автоматическая при заходе на сайт
	function action_index()
	{
        if(isset($_POST['name']))
        {

            $this->action_add();
        }
        $data = $this->model->get_data();		

		$this->view->generate('Messages_view.php', 'template_view.php', $data);
	}
    //ручная загрузка
    function action_add()
	{
        //tecdate
        date_default_timezone_set('Europe/Moscow');
        $rd = date('Y-m-d H:i:s');

		$this->model->add_data($_POST,$rd);	

	}


}
?>