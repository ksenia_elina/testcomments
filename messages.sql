-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 29 2020 г., 17:05
-- Версия сервера: 5.6.41
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `messages`
--

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date` timestamp(6) NOT NULL,
  `text` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `name`, `date`, `text`) VALUES
(1, 'Ksu', '2020-09-28 10:57:47.781999', 'Все работает'),
(2, 'Александра', '0000-00-00 00:00:00.000000', '123'),
(3, 'Александра', '0000-00-00 00:00:00.000000', '123'),
(4, 'Александра', '0000-00-00 00:00:00.000000', '123'),
(5, 'Slimpay', '0000-00-00 00:00:00.000000', '123444'),
(6, 'phpJsonSlim', '0000-00-00 00:00:00.000000', '1'),
(7, 'phpJsonSlim', '0000-00-00 00:00:00.000000', '1'),
(8, 'phpJsonSlim', '2020-09-28 12:45:40.000000', '1'),
(9, 'phpJsonSlim', '2020-09-28 12:45:49.000000', '1'),
(11, 'Slimpay', '2020-09-29 13:16:35.000000', '&lt;script&gt;&lt;/script&gt;'),
(12, 'Александра', '2020-09-29 13:17:02.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(13, 'Александра', '2020-09-29 13:19:57.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(14, 'Александра', '2020-09-29 13:20:09.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(15, 'Александра', '2020-09-29 13:20:42.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(16, 'Александра', '2020-09-29 13:21:44.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(17, 'Александра', '2020-09-29 13:22:22.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(18, 'Александра', '2020-09-29 13:22:57.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(19, 'Александра', '2020-09-29 13:27:21.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(20, 'Александра', '2020-09-29 13:27:43.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(21, 'Александра', '2020-09-29 13:32:19.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(22, 'Александра', '2020-09-29 13:32:33.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(23, 'Александра', '2020-09-29 13:32:51.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(24, 'Александра', '2020-09-29 13:33:13.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(25, 'Александра', '2020-09-29 13:33:30.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(26, 'Александра', '2020-09-29 13:33:45.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(27, 'Александра', '2020-09-29 13:34:14.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(28, 'Александра', '2020-09-29 13:37:29.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(29, 'Александра', '2020-09-29 13:38:40.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(30, 'Александра', '2020-09-29 13:49:05.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(31, 'Александра', '2020-09-29 13:50:18.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(32, 'Александра', '2020-09-29 13:51:00.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(33, 'Александра', '2020-09-29 13:51:33.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;'),
(34, 'Александра', '2020-09-29 13:52:43.000000', '&lt;script&gt;alert(\'broken\')&lt;/script&gt;');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
